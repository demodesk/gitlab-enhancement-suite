const GITLAB_QUERY_SELECTORS = {
  ACTION_BUTTON_CONTAINER: '.detail-page-header-actions',
  TICKET_TITLE: '[data-testid="issue-title"]',
  MR_TITLE: '[data-testid="title-content"]',
  TICKET_OR_MR_NUMBER_LINK: '.gl-breadcrumb-item:last-of-type',
};

const ENHANCER_CLASSES = {
  COPY_BUTTON: 'dd-gitlab-enhancer-copy-button',
  COPY_TARGET: 'dd-gitlab-enhancer-copy-target',
}

function createCopyButton() {
  const button = document.createElement('button');
  button.innerText = 'Copy link';
  button.className = `${ENHANCER_CLASSES.COPY_BUTTON} btn gl-display-none gl-sm-display-block btn-default btn-md gl-button`;

  button.addEventListener('click', (e) => {
    e.preventDefault();

    console.log('[DEMODESK_GITLAB_ENHANCEMENT]', 'Copying to clipboard...')
    const copyTarget = document.querySelector(`.${ENHANCER_CLASSES.COPY_TARGET}`);

    copyElementToClipboard(copyTarget);

    // success message
    document.querySelector(`.${ENHANCER_CLASSES.COPY_BUTTON}`).innerText = 'Copied!';
    setTimeout(() => {
      document.querySelector(`.${ENHANCER_CLASSES.COPY_BUTTON}`).innerText = 'Copy link';
    }, 2000);
  });

  if (isMergeRequestPage()) {
    button.style.marginRight = '8px';
  }

  return button;
}

function injectCopyButton() {
  let container = document.querySelector(GITLAB_QUERY_SELECTORS.ACTION_BUTTON_CONTAINER);
  if (container.length === 0) {
    console.warn(
      '[DEMODESK_GITLAB_ENHANCEMENT]',
      'Could not find the action button container.',
      `Looked for ${GITLAB_QUERY_SELECTORS.ACTION_BUTTON_CONTAINER}`
    );
  } else if (container.length > 1) {
    container = container[0];
  }

  const copyTarget = createHiddenCopyTarget();
  const button = createCopyButton();

  container.appendChild(copyTarget);
  container.prepend(button);
}

function createHiddenCopyTarget() {
  // We create a "hidden" ahref with the link and title of the ticket that we then copy on button press
  const container = document.createElement('div');
  container.className = ENHANCER_CLASSES.COPY_TARGET;
  container.innerText = ' :gitlab:'; // slack will show the gitlab emoji automatically
  // we can't use visibility: hidden or display: none because it wouldn't copy it then
  container.style = "position: absolute; left: -1000px; top: -1000px";

  const ahref = document.createElement('a');
  container.prepend(ahref);
  ahref.href = window.location.href;

  if (isTicketPage()) {
    ahref.innerText = document.querySelector(GITLAB_QUERY_SELECTORS.TICKET_TITLE).innerText + ` (${getTicketOrMRLabel()})`;
  } else if (isMergeRequestPage()) {
    ahref.innerText = document.querySelector(GITLAB_QUERY_SELECTORS.MR_TITLE).innerText + ` (${getTicketOrMRLabel()})`;
  } else {
    console.warn('[DEMODESK_GITLAB_ENHANCEMENT]', 'Could not find the ticket/MR title.');
  }

  return container;
}

// Copies the HTML of the element to clipboard. Works best with prose elements like ahrefs or images
function copyElementToClipboard(element) {
  window.getSelection().removeAllRanges();
  let range = document.createRange();
  range.selectNode(typeof element === 'string' ? document.getElementById(element) : element);
  window.getSelection().addRange(range);
  document.execCommand('copy');
  window.getSelection().removeAllRanges();
}

function isTicketPage() {
  return window.location.href.includes('/issues/');
}

function isMergeRequestPage() {
  return window.location.href.includes('/merge_requests/');
}

function getTicketOrMRLabel() {
  return document.querySelector(GITLAB_QUERY_SELECTORS.TICKET_OR_MR_NUMBER_LINK).innerText;
}


injectCopyButton();
