const STORAGE_KEYS = {
  GITLAB_PERSONAL_ACCESS_TOKEN: 'gitlabPersonalAccessToken',
};

async function readKeyFromStorage(key, defaultValue = '') {
  return new Promise((resolve => {
    chrome.storage.sync.get({ [key]: defaultValue }, items => {
      return resolve(items[STORAGE_KEYS.GITLAB_PERSONAL_ACCESS_TOKEN]);
    });
  }));
}
