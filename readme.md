## Features
1. Issue cards show which users have already approved or are currently reviewing the related MR (more details below in FAQ)
2. Opening the related MR of an issue with a single click from the issue board by clicking on the approval info mention above
3. A "Copy link" button on tickets and MRs that creates nicely formatted links for Slack

## How to use the Chrome extension?
1. Download the latest `.crx` file from [here](https://gitlab.com/demodesk/gitlab-enhancement-suite/-/tree/master/releases)
2. Open `chrome://extensions` in your Chrome browser
3. Drag the `.crx` file into the window
4. Click "Add extension" to confirm the installation
5. Click on the icon of the extension (Gitlab logo with a green plus symbol)
6. Enter a Gitlab personal access token and click "Save" (the token is needed for access to the Gitlab API and is stored in Chrome storage)
7. Open a Gitlab issue board of one of your projects or groups
8. The issue cards are now enhanced with additional information

## Screenshots
![alt text](https://gitlab.com/demodesk/gitlab-enhancement-suite/-/raw/master/screenshots/screenshot.png "Screenshot of Gitlab issue board using Gitlab Enhancement Suite")

## FAQ
1. Which MR is used for an issue when there are multiple?
   - **Answer:** MRs are considered that are related to an issue. An open MR will always be preferred over an MR in one of the other states (closed, merged, locked). If there are multiple open MRs, the most recently created one will be used.

## How to publish a new version
1. Update the version number in `manifest.json`
2. Create a new crx file using "Pack extension" in chrome://extensions
    - Select the `src` folder
    - Click "Pack extension"
    - Move the created `.crx` file to the `releases` folder
4. Chrome
   - Create a zip file of the `src` folder (test whether zipping _the content of src/_ next time)
   - Upload it to the [Chrome Web Store](https://chrome.google.com/u/0/webstore/devconsole/00d92691-2baf-4947-836e-dcaef4cb269b/settings)
   - The publisher account is gitlab.enhancement.suite@gmail.com, credentials are in LastPass
5. Firefox
   - Create a zip file of _the content_ of the `src` folder
   - Upload it on [addons.mozilla.org](https://addons.mozilla.org)
   - The publisher account is gitlab.enhancement.suite@gmail.com, credentials are in LastPass

## Development
- Chrome: Load as unpacked extension (`chrome://extensions/` -> `Load unpacked` select the `src/` folder)
- Firefox: Go to `about:debugging#/runtime/this-firefox` -> `Load Temporary Add-on...` select the `manifest.json` file
