const GITLAB_IDS = {
  BOARD_APP: 'board-app',
};

const GITLAB_CLASSES = {
  BOARDS_LIST: 'boards-list',
  BOARD_CARD: 'board-card',
  BOARD_CARD_ITEM_PATH: 'board-item-path',
  BOARD_CARD_NUMBER: 'board-card-number',
  BOARD_CARD_INFO_ITEMS: 'board-info-items',
  BOARD_CARD_INFO: 'board-card-info',
  BOARD_CARD_INFO_ICON: 'board-card-info-icon',
};

const NEW_CLASSES = {
  BOARD_CARD_ENHANCED: 'board-card-enhanced',
  BOARD_CARD_APPROVED_BY: 'board-card-approved-by',
  REVIEWER_AVATAR: 'reviewer-avatar',
  REVIEWER_AVATAR_APPROVED: 'reviewer-avatar-approved',
  REVIEWER_AVATAR_IN_REVIEW: 'reviewer-avatar-in-review',
};

const ASSETS = {
  APPROVAL: './images/approval.svg',
};

const EXTENSION_SETTINGS = {
  NAME: 'Gitlab Enhancement Suite',
};

const USER_SETTINGS = {
  PROJECT_ID: document.body.dataset.projectId, // only defined for project boards (not group boards)
  GROUP_BOARD_SHOWN: document.body.dataset.page === 'groups:boards:show',
  GITLAB_PERSONAL_ACCESS_TOKEN_PROMISE: readKeyFromStorage(STORAGE_KEYS.GITLAB_PERSONAL_ACCESS_TOKEN),
};

const GITLAB_API_URL = 'https://gitlab.com/api/v4';

const ENDPOINTS = {
  LIST_MERGE_REQUESTS_RELATED_TO_ISSUE: (projectId, issueIID) => (
    `${GITLAB_API_URL}/projects/${projectId}/issues/${issueIID}/related_merge_requests`
  ),
  GET_MERGE_REQUEST_APPROVAL_STATUS: (projectId, mergeRequestIID) => (
    `${GITLAB_API_URL}/projects/${projectId}/merge_requests/${mergeRequestIID}/approvals`
  ),
  GET_MERGE_REQUEST_REVIEWERS: (projectId, mergeRequestIID) => (
    `${GITLAB_API_URL}/projects/${projectId}/merge_requests/${mergeRequestIID}/reviewers`
  ),
};

const ERRORS = {
  GITLAB_PERSONAL_ACCESS_TOKEN_MISSING: (
    `${EXTENSION_SETTINGS.NAME}: Your personal access token is missing. Please click on the extension icon to enter it.`
  ),
  GITLAB_PROJECT_AND_GROUP_BOARDS_SUPPORTED_ONLY: (
    `${EXTENSION_SETTINGS.NAME}: Only project and group boards are supported.`
  ),
  FETCHING_MERGE_REQUESTS_FOR_ISSUE_FAILED: (issueIID) => (
    `${EXTENSION_SETTINGS.NAME}: Fetching merge requests for issue #${issueIID} failed.`
  ),
  FETCHING_MERGE_REQUEST_APPROVAL_STATUS_FAILED: (mergeRequestIID) => (
    `${EXTENSION_SETTINGS.NAME}: Fetching approvals for merge request !${mergeRequestIID} failed.`
  ),
  FETCHING_MERGE_REQUEST_REVIEWERS_FAILED: (mergeRequestIID) => (
    `${EXTENSION_SETTINGS.NAME}: Fetching reviewers for merge request !${mergeRequestIID} failed.`
  ),
  APPROVED_BY_COULD_NOT_BE_ADDED: (issueIID) => (
    `${EXTENSION_SETTINGS.NAME}: Adding approved by info for issue #${issueIID} was not possible.`
  ),
};

const FIELDS = {
  MERGE_REQUEST: {
    IID: 'iid',
    STATE: 'state',
    CREATED_AT: 'created_at',
  },
  MERGE_REQUEST_APPROVAL_STATUS: {
    APPROVED_BY: 'approved_by',
  },
  USER: {
    ID: 'id',
    AVATAR_URL: 'avatar_url',
  },
  REVIEWER: {
    STATE: 'state',
  },
}

const MERGE_REQUEST_STATES = {
  OPENED: 'opened',
};

if (USER_SETTINGS.PROJECT_ID || USER_SETTINGS.GROUP_BOARD_SHOWN) {
  checkGitlabPersonalAccessTokenPresence().then(observeBoardsList).catch(console.error);
} else {
  console.info(ERRORS.GITLAB_PROJECT_AND_GROUP_BOARDS_SUPPORTED_ONLY);
}

function checkGitlabPersonalAccessTokenPresence() {
  return getFetchOptions();
}

async function getFetchOptions() {
  return new Promise((async (resolve, reject) => {
    const gitlabPersonalAccessToken = await USER_SETTINGS.GITLAB_PERSONAL_ACCESS_TOKEN_PROMISE;

    if (!gitlabPersonalAccessToken) reject(ERRORS.GITLAB_PERSONAL_ACCESS_TOKEN_MISSING);

    resolve({
      headers: {
        'Authorization': `Bearer ${gitlabPersonalAccessToken}`,
      },
    });
  }));
}

function observeBoardsList() {
  const boardsListElement = document.querySelector(`.${GITLAB_CLASSES.BOARDS_LIST}`);

  const observer = new MutationObserver(mutations => {
    for (const mutation of mutations) {
      if (mutation.type === 'childList') {
        Array.from(mutation.addedNodes)
          .filter(el => (el.classList &&
            el.classList.contains(GITLAB_CLASSES.BOARD_CARD) &&
            !el.classList.contains(NEW_CLASSES.BOARD_CARD_ENHANCED)
          ))
          .forEach(enhanceBoardCard);
      }
    }
  });

  observer.observe(boardsListElement, {
    childList: true,
    subtree: true,
    attributes: false,
  });
}

async function enhanceBoardCard(boardCard) {
  const issueIID = getIssueIID(boardCard);
  const projectId = USER_SETTINGS.PROJECT_ID || getURLEncodedProjectPath(boardCard);
  const latestMergeRequestIID = await getLatestMergeRequestIID(projectId, issueIID);

  // if the issue has a merge request related to it
  if (latestMergeRequestIID) {
    await addReviewersToBoardCard(boardCard, projectId, issueIID, latestMergeRequestIID);
  }

  boardCard.classList.add(NEW_CLASSES.BOARD_CARD_ENHANCED);
}

function getIssueIID(boardCard) {
  const boardCardNumberElement = boardCard.querySelector(`.${GITLAB_CLASSES.BOARD_CARD_NUMBER}`);
  return getDirectInnerText(boardCardNumberElement).replace(/[^0-9]/g, '');
}

function getDirectInnerText(element) {
  return Array.prototype.filter
    .call(element.childNodes, child => child.nodeType === Node.TEXT_NODE)
    .map(child => child.textContent)
    .join('');
}

function getURLEncodedProjectPath(boardCard) {
  const issuePathElement =
    boardCard.querySelector(`.${GITLAB_CLASSES.BOARD_CARD_ITEM_PATH}`) ||
    boardCard.querySelector(`.${GITLAB_CLASSES.BOARD_CARD_NUMBER} > span:last-of-type`); // fallback
  return encodeURIComponent(issuePathElement.title);
}

async function getLatestMergeRequestIID(projectId, issueIID) {
  const mergeRequestsRelatedToIssue =
    await fetch(ENDPOINTS.LIST_MERGE_REQUESTS_RELATED_TO_ISSUE(projectId, issueIID), await getFetchOptions())
      .then(response => response.json())
      .catch(e => {
        console.error(ERRORS.FETCHING_MERGE_REQUESTS_FOR_ISSUE_FAILED, e);
        return [];
      });

  if (mergeRequestsRelatedToIssue.length === 0) return;

  const latestMergeRequest = mergeRequestsRelatedToIssue.sort((mergeRequest, otherMergeRequest) => {
    if (mergeRequest[FIELDS.MERGE_REQUEST.STATE] === MERGE_REQUEST_STATES.OPENED &&
      otherMergeRequest[FIELDS.MERGE_REQUEST.STATE] !== MERGE_REQUEST_STATES.OPENED) {
      return -1; // MRs in state "opened" take precedence
    }
    return mergeRequest[FIELDS.MERGE_REQUEST.CREATED_AT] - otherMergeRequest[FIELDS.MERGE_REQUEST.CREATED_AT];
  })[0];

  return latestMergeRequest[FIELDS.MERGE_REQUEST.IID];
}

function getApprovedBy(projectId, mergeRequestIID) {
  return fetch(ENDPOINTS.GET_MERGE_REQUEST_APPROVAL_STATUS(projectId, mergeRequestIID))
    .then(response => response.json())
    .catch(() => {
      throw Error(ERRORS.FETCHING_MERGE_REQUEST_APPROVAL_STATUS_FAILED(mergeRequestIID));
    })
    .then(approvalStatus => approvalStatus[FIELDS.MERGE_REQUEST_APPROVAL_STATUS.APPROVED_BY]);
}

function getAllReviewers(projectId, mergeRequestIID) {
  return fetch(ENDPOINTS.GET_MERGE_REQUEST_REVIEWERS(projectId, mergeRequestIID))
    .then(response => response.json())
    .catch(() => {
      throw Error(ERRORS.FETCHING_MERGE_REQUEST_REVIEWERS_FAILED(mergeRequestIID));
    });
}

async function addReviewersToBoardCard(boardCard, projectId, issueIID, mergeRequestIID) {
  try {
    const approvedBy = await getApprovedBy(projectId, mergeRequestIID);
    const approvedByUserIds = approvedBy.map(approver => approver.user[FIELDS.USER.ID]);
    const approvedByUserAvatarUrls = approvedBy.map(approver => approver.user[FIELDS.USER.AVATAR_URL])

    const allReviewers = await getAllReviewers(projectId, mergeRequestIID);
    const reviewers = allReviewers.filter(reviewer => !approvedByUserIds.includes(reviewer.user[FIELDS.USER.ID]));
    const reviewersUserAvatarUrls = reviewers.map(reviewer => reviewer.user[FIELDS.USER.AVATAR_URL]);

    const approvedByBoardCardInfoElement = createApprovedByBoardCardInfoElement(projectId, mergeRequestIID);
    [
      createApprovalImageElement(),
      ...approvedByUserAvatarUrls.map(url => (
        createAvatarImageElement(url, NEW_CLASSES.REVIEWER_AVATAR_APPROVED, 'Approved')
      )),
      ...reviewersUserAvatarUrls.map(url => (
        createAvatarImageElement(url, NEW_CLASSES.REVIEWER_AVATAR_IN_REVIEW, 'In review')
      )),
    ].forEach(element => {
      approvedByBoardCardInfoElement.appendChild(element);
    });

    const boardCardInfoItemsElement = boardCard.querySelector(`.${GITLAB_CLASSES.BOARD_CARD_INFO_ITEMS}`);
    boardCardInfoItemsElement.appendChild(approvedByBoardCardInfoElement);

  } catch (e) {
    console.error(ERRORS.APPROVED_BY_COULD_NOT_BE_ADDED(issueIID), e);
  }
}

function createApprovedByBoardCardInfoElement(projectId, mergeRequestIID) {
  const approvedByBoardCardInfoElement = document.createElement('a');
  approvedByBoardCardInfoElement.classList.add(GITLAB_CLASSES.BOARD_CARD_INFO, NEW_CLASSES.BOARD_CARD_APPROVED_BY);
  approvedByBoardCardInfoElement.href = generateMergeRequestHref(projectId, mergeRequestIID);
  approvedByBoardCardInfoElement.target = '_blank';
  return approvedByBoardCardInfoElement;
}

function generateMergeRequestHref(projectId, mergeRequestIID) {
  // project board
  if (USER_SETTINGS.PROJECT_ID) {
    return `../merge_requests/${mergeRequestIID}`;
  }
  // group board
  else {
    return `../../../../${decodeURIComponent(projectId)}/-/merge_requests/${mergeRequestIID}`;
  }
}

function createApprovalImageElement() {
  const approvalImageElement = document.createElement('img');
  approvalImageElement.src = chrome.runtime.getURL(ASSETS.APPROVAL);
  approvalImageElement.width = 17;
  approvalImageElement.height = 17;
  approvalImageElement.classList.add(GITLAB_CLASSES.BOARD_CARD_INFO_ICON);
  return approvalImageElement;
}

function createAvatarImageElement(avatarUrl, className, title) {
  const avatarImageElement = document.createElement('img');
  avatarImageElement.src = avatarUrl;
  avatarImageElement.width = 14;
  avatarImageElement.height = 14;
  avatarImageElement.title = title;
  avatarImageElement.classList.add(NEW_CLASSES.REVIEWER_AVATAR);
  avatarImageElement.classList.add(className);
  return avatarImageElement;
}
