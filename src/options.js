const ELEMENT_IDS = {
  GITLAB_PERSONAL_ACCESS_TOKEN_INPUT: 'gitlab-personal-access-token',
  STATUS: 'status',
  SAVE: 'save',
};

const TEXTS = {
  SETTINGS_SAVED: 'Settings saved.',
};

async function loadOptions() {
  const input = document.getElementById(ELEMENT_IDS.GITLAB_PERSONAL_ACCESS_TOKEN_INPUT);
  input.value = await readKeyFromStorage(STORAGE_KEYS.GITLAB_PERSONAL_ACCESS_TOKEN);
}

function saveOptions() {
  const input = document.getElementById(ELEMENT_IDS.GITLAB_PERSONAL_ACCESS_TOKEN_INPUT);
  chrome.storage.sync.set({ [STORAGE_KEYS.GITLAB_PERSONAL_ACCESS_TOKEN]: input.value }, showSaved);
}

function showSaved() {
  const status = document.getElementById(ELEMENT_IDS.STATUS);
  status.textContent = TEXTS.SETTINGS_SAVED;
  setTimeout(() => status.textContent = '', 1500);
}

document.addEventListener('DOMContentLoaded', loadOptions);
document.getElementById(ELEMENT_IDS.SAVE).addEventListener('click', saveOptions);
